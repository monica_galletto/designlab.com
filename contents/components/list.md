---
name: List
description: A list consists of related pieces of information grouped together, clearly associated to each other. Lists are easy to read and maintain, and they provide a good structural point of view for interface elements.
related:
  - navigation
  - card
  - tabs
  - breadcrumb
  - dropdown
  - sorting
---

## Examples

<todo>Add example.</todo>

[View in Pajamas UI Kit →](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Component-library?node-id=425%3A128)

## Structure

<todo>Add structure image.</todo>

## Guidelines

### When to use

- A list must contain one or more list elements.
- Arrange items in a logical way. For example, in alphabetical or numeric order, or according to a specific use case. Lists can also sometimes be rearranged based on a users' preference if a [sort](/components/sorting) component is available.
- Use a list to group a continuous set of text and images. For example, a list of members of a project.

### When not to use

<todo>Add when not to use.</todo>

### Variants

- **Unordered list**: The order of list items is not strict. List items are marked with plain bullets, which may be omitted in the UI. Uses the `<ul>` tag.
- **Ordered list**: The order of list items is strict. List items are marked with numbers. Uses the `<ol>` tag.
- **Definition list**: The order of list items matches that of a dictionary. Uses the `<dl>` tag.

### Content

- List items can have single or multiple lines of text, with a maximum of three lines.
- A list contains a single column of content, and each list item is placed in an individual row.
- Lists are commonly laid out vertically, but the items can be positioned horizontally depending on CSS styles. For example, see [Tabs](/components/tabs).

### Accessibility

<todo>Add accessibility guidelines.</todo>
